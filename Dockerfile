FROM josan2109/docker-base

# Configure SSH - port 2222
COPY ./conf/sshd_config /etc/ssh
RUN rm -f /etc/service/sshd/down
# Regenerate SSH host keys. 
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

# Create user
ARG user=usertest
RUN useradd -m -d /home/${user} -s /bin/bash ${user}
RUN sudo usermod -aG sudo ${user}
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN echo "${user}:${user}" | chpasswd

#Install Angular CLI
RUN curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo bash -
RUN apt-get -y update
RUN apt install nodejs
RUN npm install npm@latest -g
RUN npm install -g @angular/cli
