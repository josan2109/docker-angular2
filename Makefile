# from https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db

APP_NAME=99-test-angular2

# DOCKER TASKS
# Build the container
# docker build -t $(APP_NAME) .
build: ## Build the container
	docker build -t $(APP_NAME) --build-arg user=$(USER) .

run: ## Run container on port configured in `config.env` 
	docker run  -dit --name="$(APP_NAME)" -p 2222:2222 -p 4200:4200 $(APP_NAME) /usr/sbin/sshd -D

rm: ## Remove imagen
	docker rm $(APP_NAME)
stop:
	docker stop $(APP_NAME)
	docker rm $(APP_NAME)
clean: ## Clean imagen
	docker rmi $(APP_NAME)
